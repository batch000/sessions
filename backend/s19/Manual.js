/*
===============================
S15 - Javascript - Syntax, Variables, and Simple Operations
===============================
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/1wkhdUllk3oss-KLwmHxtIgnUa0ktfAkUHd6CKu9qQk8/edit#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/frontend/s11

Other References:
	MDN Web Docs - JavaScript
		https://developer.mozilla.org/en-US/docs/Web/JavaScript
	JavaScript console.log() Method
		https://developer.mozilla.org/en-US/docs/Web/API/console/log
	Variables and Memory Locations
		https://www.cs.usfca.edu/~wolber/courses/110/lectures/variables.htm
	JavaScript let Statement
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/let
	JavaScript const Statement
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/const
	JavaScript String
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String
	Single VS Double Quotes For Strings In JavaScript
		https://flexiple.com/double-vs-single-quotes-javascript/
	JavaScript Escape Characters
		https://www.tutorialspoint.com/escape-characters-in-javascript
	JavaScript Number
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number
	JavaScript Boolean
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Boolean
	JavaScript Arrays
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array
	JavaScript Objects
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object
	JavaScript Null
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/null
	JavaScript Undefined
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/undefined
	Creating Git Projects:
		GitLab
			https://gitlab.com/projects/new#blank_project
		GitHub
			https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
==========
Discussion
========== 
*/

/*
1. Create a folder named backend.

	In the backend folder, Create an "index.html" file.

	Batch Folder > backend > S15  > Discussion > index.html
*/

		/*
		<!DOCTYPE HTML>
		<html>
		    <head>
		        <title>JavaScript - Syntax, Variables, and Simple Operations</title>
		    </head>
		    <body>
		    </body>
		</html>
		*/


		
		//JavaScript renders web pages in an interactive and dynamic fashion. Meaning, it enables you to create dynamically updating content, control multimedia, animate images
		//Let get started by introducing the basic syntax elements of JavaScript.


		// [SECTION] Syntax, Statements and Comments

		
		//Statements:

		// Statements in programming are instructions that we tell the computer to perform
		// JS statements usually end with semicolon (;)
		// Semicolons are not required in JS, but we will use it to help us train to locate where a statement ends
		// A syntax in programming, it is the set of rules that describes how statements must be constructed
		// All lines/blocks of code should be written in a specific manner to work. This is due to how these codes were initially programmed to function and perform in a certain manner
/*
2. Let's try to make a simple alert in our html file. To show that we can also incorporate javascript inside of html file.
	Application > index.html
*/

		/*
		Where To Place JavaScript
			- Inline You can place JavaScript right into the HTML page using the script tags. This is good for very small sites and testing only. The inline approach does not scale well, leads to poor organization, and code duplication.
			-	External File A better approach is to place JavaScript into separate files and link to them from the HTML page. This way a single script can be included across thousands of HTML pages, and you only have one place to edit your JavaScript code. This approach is also much easier to maintain, write, and debug.
		
		Use of the Script Tag
			In the past, we had to worry about specifying many attributes for the script tag. 

		Where should I place the Script Tags?
			The script tags can go anywhere on the page, but as a best practice, many developers will place it just before the closing body tag on the HTML page. This provides faster speed load times for your web page.

*/

/*
		<!DOCTYPE HTML>
		<html>
		    <head>
		        <title>JavaScript - Syntax, Variables, and Simple Operations</title>
		    </head>
		    <body>
		    	<script>
		    		alert("Hello World");
		    	</script>
		    </body>
		</html>
		
*/


/*
3. We can also type a simple statement in dev tools > Console.
	Browser > Inspect > Console
*/

		alert("Hello Again");

		//we used devtools not just to manipulate css but also allowing you to DEBUG, view messages and run JavaScript code in the console tab.


		//Whitespace (basically, spaces and line breaks) can impact functionality in many computer languages—but not in JavaScript.  In JavaScript, whitespace is used only for readability and has no functional impact.  One effect of this is that a single statement can span multiple lines.

		//This is a statement:
		console.log("Hello World");

		//This code will do the exact same thing:
		console. log ( "Hello World " ) ; 

		//And so will this:
		console.
		log
		(
			"Hello World"
		)

		/*
		Important Note:
			-Convention: Breaking up long statements into multiple lines is common practice, because it is easier to read (and prevents you from having to scroll horizontally as when reading scripts).


			-There is one case where whitespace is actually functional: newlines can be used in place of semicolons to separate statements.  But this can cause issues in fringe cases, so as a general rule semicolons should always be used.
		*/


/*
4. Create an "index.js" file.
	Application > index.js
*/

		//Comments:

		// Comments are parts of the code that gets ignored by the language
		// Comments are meant to describe the written code

		/*
		There are two types of comments:
		    1. The single-line comment denoted by two slashes
		    2.The multi-line comment denoted by a slash and asterisk 
		*/

		/*
		Important Note:
			- Alternatively, JavaScript code can be executed in the in the Google Browser's console. This can be opened in several ways:
				- All Operating systems
					- Right Click + Inspect
				- Linux and Windows
					- Ctrl + Shift + I
				- Mac
					- Option + Command + J
			- Any line/block of code added inside a JavaScript file can also be executed in the Google Browser's Console.
			- Any line/block of code entered in the console will only be available for a certain session because all the information is stored in the device's local memory.
			- Refreshing the browser will remove all previous code added in the console.
			- Refer to "references" section of this file to find the documentation for JavaScript console.log() Method.
		*/

/*
3. Link the "index.js" script file to our HTML file by adding src inside the script to link it externally. 
   Delete the alert message.
	Application > index.html
*/

		/*
		<!DOCTYPE HTML>
		<html>
		    <!-- ... -->
		    <body>
		    	<script src="./index.js"></script>
		    </body>
		</html>
		*/

		/*
		Important Note:
			- The "script" tag is commonly placed at the bottom of the HTML file right before the closing "body" tag.
			- The reason for this is because Javascript's main function in frontend development is to make our websites and applications interactive.
			- In order to achieve this, JavaScript selects/targets specific HTML elements in our application and performs a certain output.
			- It is added last to allow all HTML and CSS resources to load first before applying any JavaScript code to run.
			- Placing the "script" tag at the top the the file might result in errors because since the HTML elements have not yet been loaded when the JavaScript loads, it does not have any valid HTML elements to target/select.
		*/

/*
4. Create a variable and print it's value in the browser console.
	Application > index.js
*/

		console.log("Hello World");

		// [SECTION] Variables

		// It is used to contain data.
		// Any information that is used by an application is stored in what we call a "memory"
		// When we create variables, certain portions of a device's memory is given a "name" that we call "variables"
		// This makes it easier for us associate information stored in our devices to actual "names" about information


		// Declaring variables:

		// Declaring variables - tells our devices that a variable name is created and is ready to store data
		// Declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning the variable's value was "not defined".

		// Syntax
		    // let/const variableName;

		//let is a keyword that is usually used in declaring a variable.
		let myVariable;
		// console.log() is useful for printing values of variables or certain results of code into the Google Chrome Browser's console
		// Constant use of this throughout developing an application will save us time and builds good habit in always checking for the output of our code
		console.log(myVariable);

		// Trying to print out a value of a variable that has not been declared will return an error of "not defined"
		// The "not defined" error in the console refers to the variable not being created/defined, whereas in the previous example, the code refers to the "value" of the variable as not defined.
		// let hello;
		console.log(hello);

		// Variables must be declared first before they are used
		// Using variables before they're declared will return an error
		let hello;

		/*
		    Guides in writing variables:
		        1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
		        2. Variable names should start with a lowercase character, use camelCase for multiple words.
		        3. For constant variables, use the 'const' keyword.
		        4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.

		    Best practices in naming variables:

				1. When naming variables, it is important to create variables that are descriptive and indicative of the data it contains.

					let firstName = "Michael"; - good variable name
					let pokemon = 25000; - bad variable name

				2. When naming variables, it is better to start with a lowercase letter. We usually avoid creating variable names that starts with capital letters. Because there are several keywords in JS that start in capital letter.

					let FirstName = "Michael"; - bad variable name
					let firstName = "Michael"; - good variable name

				3. Do not add spaces to your variable names. Use camelCase for multiple words, or underscores.

					let first name = "Mike";

				camelCase is when we have first word in small caps and the next word added without space but is capitalized:

					lastName emailAddress mobileNumber

				Underscores sample:

				let product_description = "lorem ipsum"
				let product_id = "250000ea1000"

		*/


		/*
		Important Note:
			- For every line/block of code added in the discussion, add a console.log() Method to print out the output.
			- This will visually demonstrate to the students what the codes do.
			- This will also build a good habit of checking the output which applies not just to Javascript but for all programming languages.
			- This helps build character that will promote growth as a developer ensuring that every step of the development everything is working as intended as opposed to the bad habit of inputting lines and blocks of codes without testing them and eventually having difficulty tracing the error due to the numerous additions to the source code.
			- Refer to "references" section of this file to find the documentation for Variables and Memory Locations.
		*/

/*
5. Create additional variables to demonstrate the variable declaration and initialization.
	Application > index.js
*/

		/*...*/
		// let hello;

		// Declaring and initializing variables
		// Initializing variables - the instance when a variable is given it's initial/starting value
		// Syntax
		    // let/const variableName = value;
		let productName = 'desktop computer';
		console.log(productName);

		let productPrice = 18999;
		console.log(productPrice);

		// In the context of certain applications, some variables/information are constant and should not be changed
		// In this example, the interest rate for a loan, savings account or a mortgage must not be changed due to real world concerns
		// This is the best way to prevent applications from suddenly breaking or performing in ways that are not intended
		const interest = 3.539;


		//Let's expound the used of let and const
		//let and const are the keywords that we used to declare a variables.

		//let - we usually used let if we want to reassign the values in our variable


		// Reassigning variable values
		// Reassigning a variable means changing it's initial or previous value into another value
		// Syntax
		    // variableName = newValue;

		//in our example, we already initialized a value with the used of let in productName variable. Let's try to re assign it with new value.
		productName = 'Laptop';
		console.log(productName);

		//let variable cannot be re-declared within its scope. So while this will work:
		let friend = 'Kate';
		friend = 'Jane';

		//this will return an error
		let friend = 'Kate';
		let friend = 'Jane'; // error: Identifier 'greeting' has already been declared

	
		//while const cannot be updated or re-declared
		// Values of constants cannot be changed and will simply return an error
		//let's use the const that we declared earlier:
		// interest = 4.489;


		//So if we declare a variable with const, we can neither update nor the variable identifier cannot be reassigned

		/*
		When to use JavaScript const?
			As a general rule, always declare a variable with const unless you know that the value will change.

			Use const when you declare:

				-A new Array
				-A new Object
				-A new Function

		Note: You can discuss later the constant array and objects once done with the topic of data types. It can only be an overview to explain that the const does not define a constant value. It defines a constant reference to a value so it can change the elements in an array and properties in an object.
		*/

		// Reassigning variables vs initializing variables
		// Declares a variable first
		let supplier;
		// Initialization is done after the variable has been declared
		// This is considered as initialization because it is the first time that a value has been assigned to a variable
		supplier = "John Smith Tradings";
		console.log(supplier);
		// This is considered as reassignment because it's initial value was already declared
		supplier = "Zuitt Store";
		console.log(supplier);


		//Can you declare a const variable without initialization? No. An error will occur.
			/*
			Example: 
				const pi;
				pi=3.1416;
				console.log(pi)
			*/

		//const variables are variables with constant data. Therefore we should not re-declare,re-assign or even declare a const variable without initialization.


		/*
		Important Note:
			- Refer to "references" section of this file to find the documentations for JavaScript let and const Statements.
		*/


		//var vs. let/const

		//some of you may wonder why we used let and const in declaring a variable when we search online, we usually see var. 

		//var - is also used in declaring a variable. but var is an ECMAScript1 (ES1) feature[ES1 (JavaScript 1997)]. 
		//let/const was introduced as a new feature in ES6(2015)

		//what makes let/const different from var?

		//There are issues associated with variables declared with var, regarding with hoisting.
		//In terms of variables and constants, keyword var is hoisted and let and const does not allow hoisting.
		//Hoisting is JavaScript's default behavior of moving declarations to the top.
		

		//for example:
			a = 5;
			console.log(a);
			var a; // 5

		//In the above example, variable a is used before declaring it. And the program works and displays the output 5.

		//If a variable is used with the let keyword, that variable is not hoisted. 
			a = 5;
			console.log(a);
			let a; // error


		//let/const local/global scope
		//Scope essentially means where these variables are available for use
		//let and const are block scoped
		//A block is a chunk of code bounded by {}. A block lives in curly braces. Anything within curly braces is a block.
		//So a variable declared in a block with let  is only available for use within that block. 

		let outerVariable = 'hello';

		{
			let innerVariable = 'hello again'
		}

		console.log(outerVariable)
		console.log(innerVariable) // innerVariable is not defined


		//Like let declarations, const declarations can only be accessed within the block they were declared.

		const outerVariable = 'hello';

		{
			const innerVariable = 'hello again'
		}

		console.log(outerVariable)
		console.log(innerVariable) // innerVariable is not defined


		/*
		Important Note:
			- The use case of scopes and code blocks will be discussed further in later sessions using functions and conditional statements.
		*/

/*
5. Create additional variables to demonstrate multiple variable declaration.
	Application > index.js
*/

		/*...*/
		//console.log(innerVariable) // innerVariable is not defined

		// Multiple variable declarations
		// Multiple variables may be declared in one line
		// Though it is quicker to do without having to retype the "let" keyword, it is still best practice to use multiple "let"/"const" keywords when declaring variables
		// Using multiple keywords makes code easier to read and determine what kind of variable has been created

		// let productCode = 'DC017', productBrand = 'Dell';
		let productCode = 'DC017';
		const productBrand = 'Dell';
		console.log(productCode, productBrand);

		// Using a variable with a reserved keyword.
		const let = "hello";

		console.log(let);

/*
6. Create multiple variables to demonstrate JavaScript's primitive and commonly used data types.
	Application > index.js
*/

		/*...*/
		// console.log(let);

		// [SECTION] Data Types

		// Strings
		// Strings are a series of characters that create a word, a phrase, a sentence or anything related to creating text
		// Strings in JavaScript can be written using either a single (') or double (") quote
		// In other programming languages, only the double quotes can be used for creating strings
		let country = 'Philippines';
		let province = "Metro Manila"; 

		// Concatenating strings
		// Multiple string values can be combined to create a single string using the "+" symbol
		let fullAddress = province + ', ' + country;
		console.log(fullAddress);

		let greeting = 'I live in the ' + country;
		console.log(greeting);

		// The escape character (\) in strings in combination with other characters can produce different effects
		// "\n" refers to creating a new line in between text
		let mailAddress = 'Metro Manila\n\nPhilippines';
		console.log(mailAddress);

		// Using the double quotes along with the single quotes can allow us to easily include single quotes in texts without using the escape character
		let message = "John's employees went home early";
		console.log(message);
		message = 'John\'s employees went home early';
		console.log(message);

		// Numbers
		// Integers/Whole Numbers
		let headcount = 26;
		console.log(headcount);

		// Decimal Numbers/Fractions
		let grade = 98.7;
		console.log(grade);

		// Exponential Notation
		let planetDistance = 2e10;
		console.log(planetDistance);

		// Combining text and strings
		console.log("John's grade last quarter is " + grade);

		// Boolean
		// Boolean values are normally used to store values relating to the state of certain things
		// This will be useful in further discussions about creating logic to make our application respond to certain scenarios
		let isMarried = false;
		let inGoodConduct = true;
		console.log("isMarried: " + isMarried);
		console.log("isGoodConduct: " + inGoodConduct);

		// Arrays
		// Arrays are a special kind of data type that's used to store multiple values
		// Arrays can store different data types but is normally used to store similar data types

		// similar data types
		// Syntax
		    // let/const arrayName = [elementA, elementB, elementC, ...]
		let grades = [98.7, 92.1, 90.2, 94.6];
		console.log(grades);

		// different data types
		// storing different data types inside an array is not recommended because it will not make sense to in the context of programming
		let details = ["John", "Smith", 32, true];
		console.log(details);

		// Objects
		// Objects are another special kind of data type that's used to mimic real world objects/items
		// They're used to create complex data that contains pieces of information that are relevant to each other
		// Every individual piece of information is called a property of the object
		// Syntax
		    // let/const objectName = {
		    //     propertyA: value,
		    //     propertyB: value,
		    // }
		let person = {

		    fullName: 'Juan Dela Cruz',
		    age: 35,
		    isMarried: false,
		    contact: ["+63917 123 4567", "8123 4567"],
		    address: {
		        houseNumber: '345',
		        city: 'Manila'
		    }

		}

		console.log(person);

		// They're also useful for creating abstract objects
		let myGrades = {
		    firstGrading: 98.7, 
		    secondGrading: 92.1, 
		    thirdGrading: 90.2, 
		    fourthGrading: 94.6
		}

		console.log(myGrades);

		//typeof operator is used to determine the type of data or the value of a variable. It outputs a string.
		console.log(typeof myGrades);//object

		//Note: Array is a special type of object with methods and functions to manipulate it. We will discuss these methods in later sessions. (S22 - Javascript - Array Manipulation)
		console.log(typeof grades);


		/*
		Constant Objects and Arrays
			The keyword const is a little misleading.

			It does not define a constant value. It defines a constant reference to a value.

			Because of this you can NOT:

			Reassign a constant value
			Reassign a constant array
			Reassign a constant object

			But you CAN:

			Change the elements of constant array
			Change the properties of constant object

		*/

		//for example:

		const anime = ['one piece', 'one punch man', 'attack on titan'];
		anime = ['kimetsu no yaiba'];

		console.log(anime)// Assignment to constant variable.

		//but if we do this:
		const anime = ['one piece', 'one punch man', 'attack on titan'];
		anime[0] = ['kimetsu no yaiba'];

		console.log(anime);

		//We can change the element of an array assigned to a constant variable.
		//We can also change the object's properties assigned to a constant variable.


		/*
		Important Note:
			- Arrays and Objects will be discussed in further details during the succeeding sessions.
			- They were added in the discussion to give students an idea that they also fall under the category of JavaScript data types and are part of it's fundamentals.
			- Refer to "references" section of this file to find the documentations for JavaScript String, Single VS Double Quotes For Strings In JavaScript, JavaScript Escape Characters, JavaScript Number, JavaScript Boolean, JavaScript Arrays and JavaScript Objects.
			-We can change the element of an array assigned to a constant variable or change the object's properties assigned to a constant variable. However, we cannot re-assign a new array or object to the constant variable.
		*/

/*
7. Create additional variables to demonstrate null and undefined data types.
	Application > index.js
*/

		/*...*/

		console.log(myGrades);

		// Null
		// It is used to intentionally express the absence of a value in a variable declaration/initialization
		// null simply means that a data type was assigned to a variable but it does not hold any value/amount or is nullified
		let spouse = null;
		// Using null compared to a 0 value and an empty string is much better for readability purposes
		// null is also considered as a data type of it's own compared to 0 which is a data type of a number and single quotes which are a data type of a string
		let myNumber = 0;
		let myString = '';

		// Undefined
		// Represents the state of a variable that has been declared but without an assigned value
		let fullName;
		console.log(fullName);

		// Undefined vs Null
		// One clear difference between undefined and null is that for undefined, a variable was created but was not provided a value
		// null means that a variable was created and was assigned a value that does not hold any value/amount
		// Certain processes in programming would often return a "null" value when certain tasks results to nothing
		let varA = null;
		console.log(varA);

		// For undefined, this is normally caused by developers creating variables that have no value/data associated with them
		// This is when the value of a variable is still unknown
		let varB;
		console.log(varB);

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentations for JavaScript null and undefined.
		*/

/*
8. Create a function and demonstrate the use of functions in programming.
	Application > index.js
*/

		/*...*/
		console.log(varB);

		

/*
========
Activity
========
*/

/*
1. Create an "activity" folder, an "index.html" file inside of it and link the "index.js" file.
	Batch Folder > S15 > Activity > index.html
*/

		/*
		<!DOCTYPE HTML>
		<html>
		    <head>
		        <title>Activity: JavaScript Introduction</title>
		    </head>
		    <body>
		    	<script src="./index.js"></script>
		    </body>
		</html>
		*/

/*
2. Create an "index.js" file and log the message "Hello World" in the console to ensure that the script file is properly associated with the html file.
	Application > index.js
*/

		console.log("Hello World");


/*


3. Add the activity-template.js code and instructions in the batch Boodle Notes.

		3a. Copy and paste the code and instructions and create variables with varying data types relating to user information.

		Application > index.js
*/
		
		console.log("Hello World");

		/*
			1. Create variables to store to the following user details:

			-first name - String
			-last name - String
			-age - Number
			-hobbies - Array
			-work address - Object

				-The hobbies array should contain at least 3 hobbies as Strings.
				-The work address object should contain the following key-value pairs:

					houseNumber: <value>
					street: <value>
					city: <value>
					state: <value>

			Log the values of each variable to follow/mimic the output.

			Note:
				-Name your own variables but follow the conventions and best practice in naming variables.
				-You may add your own values but keep the variable names and values Safe For Work.
		*/
		
		let firstName = 'John';
		console.log("First Name: " + firstName);

		let lastName = 'Smith';
		console.log("Last Name: " + lastName);

		let age = 30;
		console.log("Age: " + age);

		let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
		console.log("Hobbies:");
		console.log(hobbies);

		let workAddress = {
			houseNumber: "32",
			street: "Washington",
			city: "Lincoln",
			state: "Nebraska"
		}
		console.log("Work Address:");
		console.log(workAddress);

	/*	
		3b. Identify and implement the best practices of creating and using variables by avoiding errors debugging the following codes.

		Application > index.js
	*/

		/*			
			Debugging Practice - Identify and implement the best practices of creating and using variables by avoiding errors debugging the following codes:

				Log the values of each variable to follow/mimic the output.
		*/

		let fullName = "Steve Rogers";
		console.log("My full name is: " + fullName);

		let currentAge = 40;
		console.log("My current age is: " + currentAge);
		
		let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
		console.log("My Friends are: ")
		console.log(friends);

		let profile = {

			username: "captain_america",
			fullName: "Steve Rogers",
			age: 40,
			isActive: false,

		}
		console.log("My Full Profile: ")
		console.log(profile);

		let fullName2 = "Bucky Barnes";
		console.log("My bestfriend is: " + fullName2);

		const lastLocation = "Arctic Ocean";
		//lastLocation = "Atlantic Ocean";
		console.log("I was found frozen in: " + lastLocation);

