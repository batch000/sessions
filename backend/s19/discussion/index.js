// [SECTION] Syntax, Statements and Comments
// Statements in programming are instructions that we tell the computer to perform
// JS statements usually end with semicolon (;)
// Semicolons are not required in JS, but we will use it to help us train to locate where a statement ends
// A syntax in programming, it is the set of rules that describes how statements must be constructed
// All lines/blocks of code should be written in a specific manner to work. This is due to how these codes were initially programmed to function and perform in a certain manner
console.log("Hello World");

// Comments are parts of the code that gets ignored by the language
// Comments are meant to describe the written code

/*
There are two types of comments:
    1. The single-line comment denoted by two slashes
    2.The multi-line comment denoted by a slash and asterisk 
*/

// [SECTION] Variables

// It is used to contain data.
// Any information that is used by an application is stored in what we call a "memory"
// When we create variables, certain portions of a device's memory is given a "name" that we call "variables"
// This makes it easier for us associate information stored in our devices to actual "names" about information

// Declaring variables
// Declaring variables - tells our devices that a variable name is created and is ready to store data
// Declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning the variable's value was "not defined".
// Syntax
    // let/const variableName;
let myVariable;
// console.log() is useful for printing values of variables or certain results of code into the Google Chrome Browser's console
// Constant use of this throughout developing an application will save us time and builds good habit in always checking for the output of our code
console.log(myVariable);

// Trying to print out a value of a variable that has not been declared will return an error of "not defined"
// The "not defined" error in the console refers to the variable not being created/defined, whereas in the previous example, the code refers to the "value" of the variable as not defined.
let hello;
console.log(hello);

// Variables must be declared first before they are used
// Using variables before they're declared will return an error
// let hello;

/*
    Guides in writing variables:
        1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
        2. Variable names should start with a lowercase character, use camelCase for multiple words.
        3. For constant variables, use the 'const' keyword.
        4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.
*/

// Declaring and initializing variables
// Initializing variables - the instance when a variable is given it's initial/starting value
// Syntax
    // let/const variableName = value;
let productName = 'desktop computer';
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

// In the context of certain applications, some variables/information are constant and should not be changed
// In this example, the interest rate for a loan, savings account or a mortgage must not be changed due to real world concerns
// This is the best way to prevent applications from suddenly breaking or performing in ways that are not intended
const interest = 3.539;


// Reassigning variable values
// Reassigning a variable means changing it's initial or previous value into another value
// Syntax
    // variableName = newValue;
productName = 'Laptop';
console.log(productName);

        //let variable cannot be re-declared within its scope. So while this will work:
        //let friend = 'Kate';
        //friend = 'Jane';

        //this will return an error
        //let friend = 'Kate';
        //let friend = 'Jane'; // error: Identifier 'greeting' has already been declared


// Values of constants cannot be changed and will simply return an error
// interest = 4.489;

// Reassigning variables vs initializing variables
// Declares a variable first
let supplier;
// Initialization is done after the variable has been declared
// This is considered as initialization because it is the first time that a value has been assigned to a variable
supplier = "John Smith Tradings";
console.log(supplier);
// This is considered as reassignment because it's initial value was already declared
supplier = "Zuitt Store";
console.log(supplier);

        //Can you declare a const variable without initialization? No. An error will occur.
            /*
            Example: 
                const pi;
                pi=3.1416;
                console.log(pi)
            */

//var vs. let/const


        //some of you may wonder why we used let and const in declaring a variable when we search online, we usually see var. 

        //var - is also used in declaring a variable. but var is an ECMAScript1 (ES1) feature[ES1 (JavaScript 1997)]. 
        //let/const was introduced as a new feature in ES6(2015)

        //what makes let/const different from var?

        //There are issues associated with variables declared with var, regarding hoisting.
        //Hoisting is JavaScript's default behavior of moving declarations to the top.
        //In terms of variables and constants, keyword var is hoisted and let and const does not allow hoisting.
        

        //for example:
            a = 5;
            console.log(a);
            var a; // 5

        //In the above example, variable a is used before declaring it. And the program works and displays the output 5.

        //If a variable is used with the let keyword, that variable is not hoisted. 
            //a = 5;
            //console.log(a);
            //let a; // error


        //let/const local/global scope
        //Scope essentially means where these variables are available for use
        //let and const are block scoped
        //A block is a chunk of code bounded by {}. A block lives in curly braces. Anything within curly braces is a block.
        //So a variable declared in a block with let  is only available for use within that block. 

        // let outerVariable = 'hello';

        // {
        //     let innerVariable = 'hello again'
        // }

        // console.log(outerVariable)
        // console.log(innerVariable) // innerVariable is not defined


        //Like let declarations, const declarations can only be accessed within the block they were declared.

        // const outerVariable = 'hello';

        // {
        //     const innerVariable = 'hello again'
        // }

        // console.log(outerVariable)
        // console.log(innerVariable) // innerVariable is not defined



// Multiple variable declarations
// Multiple variables may be declared in one line
// Though it is quicker to do without having to retype the "let" keyword, it is still best practice to use multiple "let"/"const" keywords when declaring variables
// Using multiple keywords makes code easier to read and determine what kind of variable has been created

// let productCode = 'DC017', productBrand = 'Dell';
let productCode = 'DC017';
const productBrand = 'Dell';
console.log(productCode, productBrand);

// Using a variable with a reserved keyword.
// const let = "hello";

// console.log(let);

// [SECTION] Data Types

// Strings
// Strings are a series of characters that create a word, a phrase, a sentence or anything related to creating text
// Strings in JavaScript can be written using either a single (') or double (") quote
// In other programming languages, only the double quotes can be used for creating strings
let country = 'Philippines';
let province = "Metro Manila"; 

// Concatenating strings
// Multiple string values can be combined to create a single string using the "+" symbol
let fullAddress = province + ', ' + country;
console.log(fullAddress);

let greeting = 'I live in the ' + country;
console.log(greeting);

// The escape character (\) in strings in combination with other characters can produce different effects
// "\n" refers to creating a new line in between text
let mailAddress = 'Metro Manila\n\nPhilippines';
console.log(mailAddress);

// Using the double quotes along with the single quotes can allow us to easily include single quotes in texts without using the escape character
let message = "John's employees went home early";
console.log(message);
message = 'John\'s employees went home early';
console.log(message);

// Numbers
// Integers/Whole Numbers
let headcount = 26;
console.log(headcount);

// Decimal Numbers/Fractions
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text and strings
console.log("John's grade last quarter is " + grade);

// Boolean
// Boolean values are normally used to store values relating to the state of certain things
// This will be useful in further discussions about creating logic to make our application respond to certain scenarios
let isMarried = false;
let inGoodConduct = true;
console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + inGoodConduct);

// Arrays
// Arrays are a special kind of data type that's used to store multiple values
// Arrays can store different data types but is normally used to store similar data types

// similar data types
// Syntax
    // let/const arrayName = [elementA, elementB, elementC, ...]
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// different data types
// storing different data types inside an array is not recommended because it will not make sense to in the context of programming
let details = ["John", "Smith", 32, true];
console.log(details);

// Objects
// Objects are another special kind of data type that's used to mimic real world objects/items
// They're used to create complex data that contains pieces of information that are relevant to each other
// Every individual piece of information is called a property of the object
// Syntax
    // let/const objectName = {
    //     propertyA: value,
    //     propertyB: value,
    // }
let person = {

    fullName: 'Juan Dela Cruz',
    age: 35,
    isMarried: false,
    contact: ["+63917 123 4567", "8123 4567"],
    address: {
        houseNumber: '345',
        city: 'Manila'
    }

}

console.log(person);

// They're also useful for creating abstract objects
let myGrades = {
    firstGrading: 98.7, 
    secondGrading: 92.1, 
    thirdGrading: 90.2, 
    fourthGrading: 94.6
}

console.log(myGrades);


//typeof operator is used to determine the type of data or the value of a variable. It outputs a string.
console.log(typeof myGrades);//object

//Note: Array is a special type of object with methods and functions to manipulate it. We will discuss these methods in later sessions. (S22 - Javascript - Array Manipulation)
console.log(typeof grades);

/*
        Constant Objects and Arrays
            The keyword const is a little misleading.

            It does not define a constant value. It defines a constant reference to a value.

            Because of this you can NOT:

            Reassign a constant value
            Reassign a constant array
            Reassign a constant object

            But you CAN:

            Change the elements of constant array
            Change the properties of constant object

        */

        //for example:

        // const anime = ['one piece', 'one punch man', 'attack on titan'] 
        // anime = ['kimetsu no yaiba']

        // console.log(anime)// Assignment to constant variable.

        //but if we do this:
        const anime = ['one piece', 'one punch man', 'attack on titan']
        anime[0] = ['kimetsu no yaiba']

        console.log(anime)

        //we can change the element of the array
        //same thing with object, we can change the object's properties.


// Null
// It is used to intentionally express the absence of a value in a variable declaration/initialization
// null simply means that a data type was assigned to a variable but it does not hold any value/amount or is nullified
let spouse = null;

// Using null compared to a 0 value and an empty string is much better for readability purposes
// null is also considered as a data type of it's own compared to 0 which is a data type of a number and single quotes which are a data type of a string
let myNumber = 0;
let myString = '';

// Undefined
// Represents the state of a variable that has been declared but without an assigned value
let fullName;
console.log(fullName);

// Undefined vs Null
// One clear difference between undefined and null is that for undefined, a variable was created but was not provided a value
// null means that a variable was created and was assigned a value that does not hold any value/amount
// Certain processes in programming would often return a "null" value when certain tasks results to nothing
let varA = null;
console.log(varA);

// For undefined, this is normally caused by developers creating variables that have no value/data associated with them
// This is when the value of a variable is still unknown
let varB;
console.log(varB);

