# WDC028 - S14 - JavaScript - Introduction

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1TiphmUqXy1uE0ftQeuzjlTNjJWBz0WY9UkestTBmqgc/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1TiphmUqXy1uE0ftQeuzjlTNjJWBz0WY9UkestTBmqgc/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s14) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/backend/s14/Manual.js) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                                             | Link                                                         |
| ------------------------------------------------- | ------------------------------------------------------------ |
| MDN Web Docs - JavaScript                         | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript) |
| JavaScript console.log() Method                   | [Link](https://developer.mozilla.org/en-US/docs/Web/API/console/log) |
| Variables and Memory Locations                    | [Link](https://www.cs.usfca.edu/~wolber/courses/110/lectures/variables.htm) |
| JavaScript let Statement                          | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/let) |
| JavaScript const Statement                        | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/const) |
| JavaScript String                                 | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String) |
| Single VS Double Quotes For Strings In JavaScript | [Link](https://flexiple.com/double-vs-single-quotes-javascript/) |
| JavaScript Escape Characters                      | [Link](https://www.tutorialspoint.com/escape-characters-in-javascript) |
| JavaScript Number                                 | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number) |
| JavaScript Boolean                                | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Boolean) |
| JavaScript Arrays                                 | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array) |
| JavaScript Objects                                | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object) |
| JavaScript Null                                   | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/null) |
| JavaScript Undefined                              | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/undefined) |
| JavaScript Functions                              | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions) |
| JavaScript return Statement                       | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/return) |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1.[5 mins] -  What is Javascript
	2.[5 mins] -  Uses of Javascript
	3.[10 mins] -  Writing comments
	4.[10 mins] -  Syntax and Statements
	5.[10 mins] -  Storing Values
		- Variables
		- Constants
	6.[10] -  Peeking at Values in Console
	7.[10] -  Console Object
	8.[1 hr 10 mins] -  Data Types
		- Strings
		- Numbers
		- Boolean
		- Null
		- Arrays
		- Object
		- Undefined
	9.[1 hr] -  Functions
	10.[1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

	1. Scopes
		- Block/local
		- Function
		- Global

[Back to top](#table-of-contents)

