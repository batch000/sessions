# Session 15 - Javascript - Syntax, Variables, and Simple Operations
# Session Objectives

At the end of the session, the students are expected to:

- learn how to create a computer program by using a programming language called JavaScript.

# Resources

## Instructional Materials

- [GitLab Repository]()
- [Google Slide Presentation]()
- [Lesson Plan](lesson-plan.md)

## Supplemental Materials

- [Uses of JavaScript](https://www.educba.com/uses-of-javascript/) (EDUCBA)
- [What is JavaScript](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/What_is_JavaScript) (MDN)

# Lesson Proper

## Introduction

JavaScript is a scripting language that enables you to make interactive web pages. JavaScript, along with HTML and CSS, forms the core technologies of the World Wide Web.

JavaScript were originally intended for web browsers. However, they are now also integrated in web servers, usually through the use of Node.js which will be tackled later in the bootcamp.

For a more in-depth introduction of JavaScript, you can look [here](https://javascript.info/intro). You can also read up on JavaScript's history [here](https://medium.com/@_benaston/lesson-1a-the-history-of-javascript-8c1ce3bffb17).

![readme-images/unnamed.gif](readme-images/unnamed.gif)

A demo of an interactive page containing an alert whenever a button is clicked.

## Uses of JavaScript

According to EDUCBA, these are the common use of JavaScript:

1. Web app development
2. Web server development
3. Browser-based game development
4. Art creation
5. Mobile applications

JavaScript is not only limited to creating software applications for the web. It can also be used to create games that you can usually play in the browser, digital artworks and even mobile applications.

# Code Discussion

## Folder and File Preparation

Create a folder named backend.

In the backend folder, create a folder named **s15**, a folder named **discussion** inside the **s15** folder, then a file named **index.html** and **index.js** inside the **discussion** folder.

## Adding Preliminary Code

Add the following code inside **index.html** file:

```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>JavaScript - Syntax, Variables, and Simple Operations</title>
    </head>
    <body>
        <script>
        </script>
    </body>
</html>
```

JavaScript can be directly added to HTML using the `<script>` tag.

## Code Discussion Proper

Add all the highlighted JavaScript code after the last added code inside the `<script>` tag. Code snippets only contain the specific code for a specified sub-topic to reduce the length of the code snippet.

After writing the code of a given section, show the output to the Console tab of a browser's DevTools window (accessible by pressing F12).

## Writing Comments

In programming, comments are important to describe the purpose of your code. JavaScript has two types of comments, inline and multi-line.

```jsx
// [SECTION] Comments

// Comments are parts of the code that gets ignored by the language.
// Comments are meant to describe the written code.

// There are two types of comments:
// - The single-line comment denoted by two slashes // 
// - The multi-line comment denoted by a slash and asterisk /**/
```

## Syntax and Comments

- Statements
    - In a programming language, statements are **instructions** that we **tell** the computer to **perform**.
    - JS statements **usually end** with **semicolon** (;).
    - Semicolons are **not required** in JS, but we will use it to help us **train** to **locate** where a statement **ends**.
- Syntax
    - In programming, it is the **set of rules** that describes **how** statements must be **constructed**.

Even though JavaScript allows the omission of a semicolon, we must train the students to add semicolons at the end of each statement so that there is an explicit marker on where a given statement ends.

## Storing Values

**Variables** and **constants** are used to contain data for various purposes such as storage, retrieval and manipulation.

A **variable** is a type of value that **can change** over the execution time of a program.

A **constant** is the opposite of variables because its value **does not change**.

A variable named productName can have a value of desktop computer.
The mathematical constant Pi has a value of 3.1416.
How can we then write these in JavaScript?

```jsx
let productName = 'desktop computer';
let productPrice = 18999;
const PI = 3.1416;
```

A variable is written by writing **let** first (and **const** for constants). It is then followed by the name of the value, followed by the equal sign (or **assignment operator**), and finally the actual value.

Let the students write the code in the **index.html** file. The code will be used in the next section.

The code above for storing values is following this format: `let/const name = value;`

## Peeking at Values Using Console

We can peek the values of a variable using our web browser.

To open the index.html file, we can either drag the file inside the web browser or press **Ctrl+O** then look for the file.

Once the file has been opened, press **F12** to open the DevTools pane.

In the DevTools pane, click the Console tab and enter the name of the variable to see its value.

## Data Types

In JavaScript, there are six types of data:

- String
- Numbers
- Boolean
- Null
- Undefined
- Object

**Strings** contain one or more alphanumeric characters.

```jsx
let country = 'Philippines';
let province = "Metro Manila";
let mailAddress = 'Metro\n\nPhilippines';
let fullAddress = province + ', ' + country;
```

Strings can be declared by using either single or double quotes.

**Numbers** include positive, negative, or numbers with decimal.

```jsx
let grade = 98.7;
let headcount = 26;
let planetDistance = 2e10;
let formula = 2^10;
```

**Boolean** contains two logical values; **true** or **false**.

```jsx
let isMarried = false;
let inGoodConduct = true;
```

**Null** indicates the absence of a value.

```jsx
let spouse = null;
let criminalRecords = null;
```

**Undefined** indicates that a variable has not been given a value yet.

```jsx
let fullName;
let secondaryAddress;
```

**Object** contains other data (like a person's parts of a full name) or a set of information (array of personal information for different persons).

```jsx
let grades = [98.7, 92.1, 90.2, 94.6];
let gradesObj = {
    firstGrading: 98.7,
    secondGrading: 92.1,
    thirdGrading: 90.2,
    fourthGrading: 94.6
};
```

String, number and boolean data types are considered as **primitive** data types because it can only contain a **single** value.

The object data type is a **composite** data type since it can either hold **multiple** data regarding a real-world/imaginary object or a **set** of data (or commonly called as **arrays**).

Lastly, null and undefined data types are considered **special** data types due to what it represents.

# Activity

Note: Copy the code from activity-template.js into the batch Boodle Notes so students can copy the template of the code for this activity.

## Instructions that can be provided to the students for reference:
1. In the S15 folder, create an activity folder, an index.html file inside of it and link the index.js file.
2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.
3. Copy the activity code from your Boodle Notes. Paste the activity code from your Boodle Notes to your index.js file.
4. Create the following variables to store to the following user details:

        Variable Name - Value Data Type:

        -firstName - String
        -lastName - String
        -age - Number
        -hobbies - Array
        -workAddress - Object
        
5. The hobbies array should contain at least 3 hobbies as Strings.
6. The work address object should contain the following key-value pairs:
    - houseNumber: <value>
    - street: <value>
    - city: <value>
    - state: <value>
7. Log the values of each variable to follow/mimic the output.
8. Identify and implement the best practices of creating and using variables by avoiding errors and debugging the following codes. 
9. Log the values of each variable to follow/mimic the output.
10. Create a git repository named backend.
11. Initialize your local backend folder as a local repository, add the remote link and push to git with the commit message of Add activity code s15.
12. Add the link in Boodle for s15.

Sample Output:
![readme-images/solution.png](readme-images/solution.png)

## Solution:

Objective 1:


```javascript

    /*
        1. Create the following variables to store to the following user details:
        Variable Name - Value Data Type:
        -firstName - String
        -lastName - String
        -age - Number
        -hobbies - Array
        -workAddress - Object

            -The hobbies array should contain at least 3 hobbies as Strings.
            -The work address object should contain the following key-value pairs:

                houseNumber: <value>
                street: <value>
                city: <value>
                state: <value>

        Log the values of each variable to follow/mimic the output..
            -You may add your own values but keep the variable names and values Safe For Work.
    */

        //Add your variables and console.log for objective 1 here:

        let firstName = 'John';
        console.log("First Name: " + firstName);

        let lastName = 'Smith';
        console.log("Last Name: " + lastName);

        let age = 30;
        console.log("Age: " + age);

        let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
        console.log("Hobbies:");
        console.log(hobbies);

        let workAddress = {
            houseNumber: "32",
            street: "Washington",
            city: "Lincoln",
            state: "Nebraska"
        }
        console.log("Work Address:");
        console.log(workAddress);
```
Objective 2:
s
```javascript
    /*          
        2. Debugging Practice - Identify and implement the best practices of creating and using variables 
           by avoiding errors and debugging the following codes:
                
            Note:
            Log the values of each variable to follow/mimic the output.
    */  

        let fullName = "Steve Rogers";
        console.log("My full name is: " + fullName);

        let currentAge = 40;
        console.log("My current age is: " + currentAge);

        let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
        console.log("My Friends are: ")
        console.log(friends);

        let profile = {

            username: "captain_america",
            fullName: "Steve Rogers",
            age: 40,
            isActive: false,

        }
        console.log("My Full Profile: ")
        console.log(profile);

        let fullName2 = "Bucky Barnes";
        console.log("My bestfriend is: " + fullName2);

        const lastLocation = "Arctic Ocean";
        //lastLocation = "Atlantic Ocean";
        console.log("I was found frozen in: " + lastLocation);
```