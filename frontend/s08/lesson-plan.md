# WDC028 - S06 - CSS Introduction

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/18MGvUSlam3JVlOi0p96pX_B4JZPwhD_IQ4oLNkp7Zjs/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/18MGvUSlam3JVlOi0p96pX_B4JZPwhD_IQ4oLNkp7Zjs/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/frontend/s06) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/frontend/s06/Manual.html) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                                                 | Link                                                         |
| ----------------------------------------------------- | ------------------------------------------------------------ |
| CSS Introduction                                      | [Link](https://www.w3schools.com/css/css_intro.asp)          |
| What is CSS                                           | [Link](https://developer.mozilla.org/en-US/docs/Learn/CSS/First_steps/What_is_CSS) |
| Getting Started with CSS                              | [Link](https://developer.mozilla.org/en-US/docs/Learn/CSS/First_steps/Getting_started) |
| CSS Naming Conventions                                | [Link](https://www.freecodecamp.org/news/css-naming-conventions-that-will-save-you-hours-of-debugging-35cea737d849/) |
| Differences between inline, internal and external CSS | [Link](https://www.hostinger.ph/tutorials/difference-between-inline-external-and-internal-css) |
| CSS Properties Reference                              | [Link](https://www.w3schools.com/cssref/)                    |
| Web Safe Fonts for HTML and CSS                       | [Link](https://www.w3schools.com/cssref/css_websafe_fonts.asp) |
| Color Picker                                          | [Link](https://htmlcolorcodes.com/color-picker/)             |
| Image Color Picker                                    | [Link](https://imagecolorpicker.com/en)                      |
| CSS Units of Measurement                              | [Link](https://www.w3schools.com/cssref/css_units.asp)       |
| CSS position Property                                 | [Link](https://www.w3schools.com/cssref/pr_class_position.asp) |
| CSS background Property                               | [Link](https://www.w3schools.com/cssref/css3_pr_background.asp) |
| CSS min-height Property                               | [Link](https://www.w3schools.com/cssref/pr_dim_min-height.asp) |
| CSS min-width Property                                | [Link](https://www.w3schools.com/cssref/pr_dim_min-width.asp) |
| CSS Pseudo-Classes                                    | [Link](https://www.w3schools.com/css/css_pseudo_classes.asp) |
| Google Fonts                                          | [Link](https://fonts.google.com/)                            |
| Font Awesome                                          | [Link](https://fontawesome.com/)                             |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1.[20 mins] -  Ways to include CSS
		a. Inline
		b. Internal / CSS Syntax
		c. External / CSS Comments
	2.[50 mins] -  CSS Selectors
		a. Universal Selector
		b. Type Selector
		c. ID Selector / Id attribute reinforcement
		d. Class Selector / Class attribute reinforcement
		e. Attribute Selector
	3.[10 mins] -  CSS Specificity Rule / !important Property
	4.[30 mins] -  CSS Combinators
		a. Adjacent Sibling Selector
		b. Descendant Selector
		c. General Sibling Selector
		d. Child Selector
	5.[1 hr] -  CSS Properties / In order of usage
		- color
		- font-family
		- font-size
		- letter-spacing
		- text-transform
		- border-width
		- border-style
		- border-color
		- height
		- width
		- border-radius
		- text-decoration
		- background-color
		- scroll-behavior
		- display
		- float
		- text-align
		- list-style-type
		- border
		- position
		- left
	6.[1 hr] -  Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

	1. CSS Naming Conventions
	2. Color Picker/Color Palette
	3. Units of Measurement
	2. Additional CSS Properties
		a. position
		b. background
		c. min-height/min-width
	2. Pseudo-Class Selectors
	3. Google Fonts
	4. Font Awesome

[Back to top](#table-of-contents)